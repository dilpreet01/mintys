<?php
defined('BASEPATH') or exit('No direct script access allowed.');

/**
 * Department Controller class
 * 
 * This controller class is used to perform department related
 * operations by admin.
 * 
 * @access public
 * @package Controller
 * @subpackage Controller
 */
class Department extends CI_Controller
{
    /**
     * Constructor function
     * 
     * This function is used for initialization.
     * 
     * @access public
     * @param void
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        check_login_user();
        $this->load->model('department_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
    }

    /**
     * Index function
     * 
     * This is a function loads by default when Department controller gets called.
     * 
     * @access public
     * @param void
     * @return void
     */
    public function index()
    {
        $data = array();
        $data['page_title'] = 'All Registered Departments';
        $data['departments'] = $this->department_model->get_all_departments();
        $data['count'] = $this->department_model->get_department_total();
        $data['main_content'] = $this->load->view('admin/department/all_departments', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    /**
     * Add function
     * 
     * This function is called to add a new department.
     * 
     * @access public
     * @param void
     * @return void
     */
    public function add()
    {
        $this->form_validation->set_rules('code', 'Code', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error_msg', 'All fields are mandatory');
            redirect(base_url('admin/department/new_department'));
        }

        $data = array(
            'code' => $this->input->post('code'),
            'description' => $this->input->post('description'),
            'type' => $this->input->post('type'),
            'added_by' => $this->session->userdata('email'),
            'created_at' => current_datetime()
        );

        $data = $this->security->xss_clean($data);

        // Check duplicate department
        $department = $this->department_model->check_department($this->input->post('code'));

        if (empty($department)) {
            $department_id = $this->department_model->insert($data, 'department');
            $this->session->set_flashdata('msg', 'Department added Successfully');
            redirect(base_url('admin/department'));
        } else {
            $this->session->set_flashdata('error_msg', 'Department already exist, try with another code');
            redirect(base_url('admin/department/new_department'));
        }
    }

    /**
     * New Department function
     * 
     * This function is called to create a new department.
     * 
     * @access public
     * @param void
     * @return void
     */
    public function new_department()
    {
        $data = array();
        $data['page_title'] = 'Create Department';
        $data['main_content'] = $this->load->view('admin/department/add', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    /**
     * Edit function
     * 
     * This function is called edit a department.
     * 
     * @access public
     * @param mixed $id
     * @return void
     */
    public function edit($id)
    {
        $data = array();
        $data['page_title'] = 'Department Edit';
        $data['department'] = $this->department_model->get_department_details($id);
        $data['main_content'] = $this->load->view('admin/department/edit', $data, TRUE);
        $this->load->view('admin/index', $data);
    }

    /**
     * Update function
     * 
     * This function is called to update a department.
     * 
     * @access public
     * @param void
     * @return void
     */
    public function update($id)
    {
        $this->form_validation->set_rules('code', 'Code', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error_msg', 'All fields are mandatory');
            redirect(base_url('admin/department/new_department'));
        }

        $data = array(
            'code' => $this->input->post('code'),
            'description' => $this->input->post('description'),
            'type' => $this->input->post('type'),
            'added_by' => $this->session->userdata('email'),
            'created_at' => current_datetime()
        );

        $data = $this->security->xss_clean($data);

        $this->department_model->update($data, $id, 'department');
        $this->session->set_flashdata('msg', 'Department updated Successfully');
        redirect(base_url('admin/department'));
    }

    /**
     * Delete function
     * 
     * This function is called to delete a department.
     * 
     * @access public
     * @param mixed $id
     * @return void
     */
    public function delete($id)
    {
        $this->department_model->delete($id);
        $this->session->set_flashdata('msg', 'Department has been deleted successfully.');
        redirect(base_url('admin/department'));
    }
}
