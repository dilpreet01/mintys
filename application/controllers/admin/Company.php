<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('Company_model');
        $this->load->model('common_model');
    }
    
    public function index(){

        $data['page_title']   = 'All Companies';
        $data['companies']    = $this->Company_model->all_companies();
        $data['main_content'] = $this->load->view('backend/companies/all-companies', $data, TRUE);
        $this->load->view('admin/index', $data);

    }

    public function add_company(){

        $this->form_validation->set_rules('company_name', 'Company Name', 'required|alpha');
        $this->form_validation->set_rules('location','Location', 'required|alpha');
        if ($this->form_validation->run()) {
            $data = array(
                 'company_name'   => $this->input->post('company_name'), 
                 'location' => $this->input->post('location')
             );
            if ($this->Company_model->add_company($data)) {
                $this->session->set_flashdata('success_msg','Company has been added successfully');
                redirect('admin/companies');
               }
               else{
                $this->session->set_flashdata('error_msg','Sorry! something went wrong');
                redirect('admin/companies');
               }
        }
        $data = array();
        $data['page_title'] = 'Add Company';
        $data['main_content'] = $this->load->view('backend/companies/add-company', $data, TRUE);
        $this->load->view('admin/index', $data);
    }


    // edit and update company

    public function edit_company($id){

        $this->form_validation->set_rules('company_name', 'Company Name', 'required|alpha');
        $this->form_validation->set_rules('location','Location', 'required|alpha');
        if ($this->form_validation->run()) {

            $data = array(
                 'company_name' => $this->input->post('company_name'), 
                 'location'     => $this->input->post('location')
             );
            // $company_id = $this->input->post('company_id');
            if ($this->Company_model->update_company($data, $id)) {
                $this->session->set_flashdata('success_msg','Company has been updated successfully');
                redirect('admin/companies');
               }
               else{
                $this->session->set_flashdata('error_msg','Sorry! something went wrong');
                redirect('admin/companies');
               }

        }

        $data = array();
        $data['page_title'] = 'Update Company';
        $data['result'] = $this->Company_model->edit_company($id);
        $data['main_content'] = $this->load->view('backend/companies/update-company', $data, TRUE);
        $this->load->view('admin/index', $data);


    }

    public function view_company($id){

        $data['page_title']   = 'View Companies';
        $data['company']    = $this->Company_model->view_company($id);
        $data['main_content'] = $this->load->view('backend/companies/view-company', $data, TRUE);
        $this->load->view('admin/index', $data);

    }
    
    // public function update_company(){

    //     $id = $this->input->post('company_id');

       


       

    // }

    public function delete($id){

       $this->Company_model->delete($id);
       $this->session->set_flashdata('delete','Company has been deleted successfully');
       redirect('admin/companies');
    }

}


