<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Group extends CI_Controller {

    public function __construct() {
	parent::__construct();
	check_login_user();
	$this->load->model('Common_model');
//	$this->load->model('backend/Common_model');

	$this->load->model('backend/Group_model');
    }

    public function index() {

	$newModel = new Group_model;
	$data = array();
	$data['page_title'] = 'All group';
	//$data['title'] = 'All Branches';
	$data['groups'] = $newModel->getDetailsNotWhere();
	$data['main_content'] = $this->load->view('backend/group/list', $data, TRUE);
	$this->load->view('admin/index', $data);
    }

    public function add() {
	$newModel = new Group_model;
	$data = array();
	$data['page_title'] = 'Add New Group';
	$this->form_validation->set_rules('group_code', 'Group Code', 'required|numeric|xss_clean');
	$this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
	if ($this->form_validation->run() === FALSE) {

	    $data['main_content'] = $this->load->view('backend/group/add', $data, TRUE);
	    $this->load->view('admin/index', $data);
	} else {
	    if ($_POST) {

		$checkCode = $newModel->getRow(['group_code' => $_POST['group_code']]);
		if (!empty($checkCode)) {
		    $this->session->set_flashdata('error_msg', 'Group code already existed');
		    redirect('administrator/group/add');
		} else {
		    $data = array(
			'group_code  ' => $_POST['group_code'],
			'description' => $_POST['description'],
			'created_by' => $_SESSION['id']
		    );
		    $data = $this->security->xss_clean($data);
		    $id = $newModel->insert($data);
		    $this->db->trans_commit();
		    $this->session->set_flashdata('msg', 'New group has been added successfully');
		    redirect('backend/group');
		}
	    }
	}
    }

    //-- update users info
    public function update($id) {
	$newModel = new Group_model;
	$data = array();
	$data['page_title'] = 'Add Update Group';
	$this->form_validation->set_rules('group_code', 'Group Code', 'required|numeric|xss_clean');
	$this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
	if ($this->form_validation->run() === FALSE) {
	    $data['group'] = $newModel->getDetail($id);
	    // print_r($data['group'] ); die;
	    $data['main_content'] = $this->load->view('backend/group/edit', $data, TRUE);
	    $this->load->view('admin/index', $data);
	} else {
	    if ($_POST) {
		//echo "uihfsa"; die;
		//print_r($_POST); die;
		$getCode = $newModel->getRow(['id' => $id]);
		$data = array(
		    'group_code  ' => $_POST['group_code'],
		    'description' => $_POST['description'],
		      'status'=>$_POST['status'],
		);
		$data = $this->security->xss_clean($data);
//print_r($data); die;
		if (!empty($getCode)) {
		    if ($getCode->group_code == $_POST['group_code']) {
			$newModel->update($data,$id);
		    } else {
			$checkCode = $newModel->getRow(['group_code' => $_POST['group_code']]);
			if (empty($checkCode)) {
			   $newModel->update($data,$id);
			} else {
			    $this->session->set_flashdata('error_msg', 'Group code already existed');
			    redirect("administrator/group/update/$id");
			}
		    }

		    $this->db->trans_commit();
		    $this->session->set_flashdata('msg', 'New group has been updated successfully');
		    redirect('administrator/group');
		}
	    }
	}
    }

    //-- delete user
    public function delete($id) {
	$newModel = new Group_model;
	$satus['status'] = '2';
	$id = $newModel->update($satus, $id);
	$this->session->set_flashdata('msg', 'Group has been deleted Successfully');
	 redirect('administrator/group');
    }

//    public function view($id) {
//
//	$newModel = new Branches_model;
//	$data = array();
//	$data['page_title'] = 'All Branches';
//	$branche = $newModel->getDetail($id);
//	if (!empty($branche)) {
//	    $data['branche'] = $branche;
//	    $data['main_content'] = $this->load->view('backend/admin_group/view', $data, TRUE);
//	    $this->load->view('admin/index', $data);
//	} else {
//	    $this->session->set_flashdata('error_msg', 'No Record');
//	    redirect('administrator/branches');
//	}
//    }
    
 
    
}
