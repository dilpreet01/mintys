<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bin extends CI_Controller {

    public function __construct() {
	parent::__construct();
	check_login_user();
	$this->load->model('Common_model');
//	$this->load->model('backend/Common_model');

	$this->load->model('backend/Bin_model');
    }

    public function index() {

	$newModel = new Bin_model;
	$data = array();
	$data['page_title'] = 'All Bins';
	//$data['title'] = 'All Branches';
	$data['bins'] = $newModel->getDetailsNotWhere();
	$data['main_content'] = $this->load->view('backend/bin/list', $data, TRUE);
	$this->load->view('admin/index', $data);
    }

    public function add() {
	$newModel = new Bin_model;
	$data = array();
	$data['page_title'] = 'Add New Bin';
	$this->form_validation->set_rules('bin_code', 'Bin Code', 'required|numeric|xss_clean');
	$this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
	if ($this->form_validation->run() === FALSE) {

	    $data['main_content'] = $this->load->view('backend/bin/add', $data, TRUE);
	    $this->load->view('admin/index', $data);
	} else {
	    if ($_POST) {

		$checkCode = $newModel->getRow(['bin_code' => $_POST['bin_code']]);
		if (!empty($checkCode)) {
		    $this->session->set_flashdata('error_msg', 'bin code already existed');
		    redirect('administrator/bin/add');
		} else {
		    $data = array(
			'bin_code  ' => $_POST['bin_code'],
			'description' => $_POST['description'],
			'created_by' => $_SESSION['id']
		    );
		    $data = $this->security->xss_clean($data);
		    $id = $newModel->insert($data);
		    $this->db->trans_commit();
		    $this->session->set_flashdata('msg', 'New bin has been added successfully');
		    redirect('backend/bin');
		}
	    }
	}
    }

    //-- update users info
    public function update($id) {
	$newModel = new Bin_model;
	$data = array();
	$data['page_title'] = 'Add Update Bin';
	$this->form_validation->set_rules('bin_code', 'Bin Code', 'required|xss_clean');
	$this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
	if ($this->form_validation->run() === FALSE) {
	    $data['bin'] = $newModel->getDetail($id);
	    // print_r($data['group'] ); die;
	    $data['main_content'] = $this->load->view('backend/bin/edit', $data, TRUE);
	    $this->load->view('admin/index', $data);
	} else {
	    if ($_POST) {
		//echo "uihfsa"; die;
		//print_r($_POST); die;
		$getCode = $newModel->getRow(['id' => $id]);
		$data = array(
		    'bin_code  ' => $_POST['bin_code'],
		    'description' => $_POST['description'],
		    'status'=>$_POST['status'],
		);
		$data = $this->security->xss_clean($data);
//print_r($data); die;
		if (!empty($getCode)) {
		    if ($getCode->bin_code == $_POST['bin_code']) {
			$newModel->update($data,$id);
		    } else {
			$checkCode = $newModel->getRow(['bin_code' => $_POST['bin_code']]);
			if (empty($checkCode)) {
			   $newModel->update($data,$id);
			} else {
			    $this->session->set_flashdata('error_msg', 'Bin code already existed');
			    redirect("administrator/bin/update/$id");
			}
		    }

		    $this->db->trans_commit();
		    $this->session->set_flashdata('msg', 'New bin has been updated successfully');
		    redirect('administrator/bin');
		}
	    }
	}
    }

    //-- delete user
    public function delete($id) {
	$newModel = new Bin_model;
	$satus['status'] = '2';
	$id = $newModel->update($satus, $id);
	$this->session->set_flashdata('msg', 'Bin has been deleted Successfully');
	 redirect('administrator/bin');
    }

//    public function view($id) {
//
//	$newModel = new Branches_model;
//	$data = array();
//	$data['page_title'] = 'All Branches';
//	$branche = $newModel->getDetail($id);
//	if (!empty($branche)) {
//	    $data['branche'] = $branche;
//	    $data['main_content'] = $this->load->view('backend/admin_group/view', $data, TRUE);
//	    $this->load->view('admin/index', $data);
//	} else {
//	    $this->session->set_flashdata('error_msg', 'No Record');
//	    redirect('administrator/branches');
//	}
//    }
    
 
    
}
