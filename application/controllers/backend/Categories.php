<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function __construct() {
	parent::__construct();
	check_login_user();
	$this->load->model('Common_model');
//	$this->load->model('backend/Common_model');

	$this->load->model('backend/Categories_model');
    }

    public function index() {

	$newModel = new Categories_model;
	$data = array();
	$data['page_title'] = 'All Categories';
	//$data['title'] = 'All Branches';
	$data['categories'] = $newModel->getDetailsNotWhere();
	$data['main_content'] = $this->load->view('backend/categories/list', $data, TRUE);
	$this->load->view('admin/index', $data);
    }

    public function add() {
	$newModel = new Categories_model;
	$data = array();
	$data['page_title'] = 'Add New Category';
	$this->form_validation->set_rules('category_code', 'Category Code', 'required|numeric|xss_clean');
	$this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
	if ($this->form_validation->run() === FALSE) {

	    $data['main_content'] = $this->load->view('backend/categories/add', $data, TRUE);
	    $this->load->view('admin/index', $data);
	} else {
	    if ($_POST) {

		$checkCode = $newModel->getRow(['category_code' => $_POST['category_code']]);
		if (!empty($checkCode)) {
		    $this->session->set_flashdata('error_msg', 'Category code already existed');
		    redirect('administrator/categories/add');
		} else {
		    $data = array(
			'category_code  ' => $_POST['category_code'],
			'description' => $_POST['description'],
			'created_by' => $_SESSION['id']
		    );
		    $data = $this->security->xss_clean($data);
		    $id = $newModel->insert($data);
		    $this->db->trans_commit();
		    $this->session->set_flashdata('msg', 'New category has been added successfully');
		    redirect('backend/categories');
		}
	    }
	}
    }

    //-- update users info
    public function update($id) {
	$newModel = new Categories_model;
	$data = array();
	$data['page_title'] = 'Add Update Category';
	$this->form_validation->set_rules('category_code', 'Category Code', 'required|numeric|xss_clean');
	$this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
	if ($this->form_validation->run() === FALSE) {
	    $data['category'] = $newModel->getDetail($id);
	  
	    $data['main_content'] = $this->load->view('backend/categories/edit', $data, TRUE);
	    $this->load->view('admin/index', $data);
	} else {
	    if ($_POST) {
		//echo "uihfsa"; die;
		//print_r($_POST); die;
		$getCode = $newModel->getRow(['id' => $id]);
		$data = array(
		    'category_code  ' => $_POST['category_code'],
		    'description' => $_POST['description'],
		      'status'=>$_POST['status'],
		);
		$data = $this->security->xss_clean($data);
//print_r($data); die;
		if (!empty($getCode)) {
		    if ($getCode->category_code == $_POST['category_code']) {
			$newModel->update($data,$id);
		    } else {
			$checkCode = $newModel->getRow(['category_code' => $_POST['category_code']]);
			if (empty($checkCode)) {
			   $newModel->update($data,$id);
			} else {
			    $this->session->set_flashdata('error_msg', 'Category code already existed');
			    redirect("administrator/categories/update/$id");
			}
		    }

		    $this->db->trans_commit();
		    $this->session->set_flashdata('msg', 'New category has been updated successfully');
		    redirect('administrator/categories');
		}
	    }
	}
    }

    //-- delete user
    public function delete($id) {
	$newModel = new Categories_model;
	$satus['status'] = '2';
	$id = $newModel->update($satus, $id);
	$this->session->set_flashdata('msg', 'Categories has been deleted Successfully');
	 redirect('administrator/categories');
    }


 
    
}
