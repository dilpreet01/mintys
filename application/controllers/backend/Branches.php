<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Branches extends CI_Controller {

    public function __construct() {
	parent::__construct();
	check_login_user();
	$this->load->model('Common_model');
	$this->load->model('backend/Branches_model');
    }

    public function index() {
	$newModel = new Branches_model;
	$data = array();
	$data['page_title'] = 'All Branches';
	//$data['title'] = 'All Branches';
	$data['branches'] = $newModel->getDetailsNotWhere();
	$data['main_content'] = $this->load->view('backend/Branches/list', $data, TRUE);
	$this->load->view('admin/index', $data);
    }

    public function add() {
	$newModel = new Branches_model;
	$data = array();
	$data['page_title'] = 'Add Branch';
	$this->form_validation->set_rules('branch_name', 'Branch Name', 'required|xss_clean');
	$this->form_validation->set_rules('VAT_no', 'VAT NO.', 'required|xss_clean');
	$this->form_validation->set_rules('physical_address', 'Physical Address', 'required|xss_clean');
	$this->form_validation->set_rules('phone', 'Phone', 'numeric|xss_clean');
	$this->form_validation->set_rules('website', 'Web Site', 'required|xss_clean');
	$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
	//$this->form_validation->set_rules('under_type', 'Under to', 'required|xss_clean');
	if ($this->form_validation->run() === FALSE) {
	    $data['companies'] = $this->Common_model->selectTable('companies');
	    $data['main_content'] = $this->load->view('backend/Branches/add', $data, TRUE);
	    $this->load->view('admin/index', $data);
	} else {
	    //   echo "kjdfhng;olsdf"; die;
	    if ($_POST) {
		$data = array(
		    'branch_name  ' => $_POST['branch_name'],
		    'VAT_no' => $_POST['VAT_no'],
		    'physical_address' => $_POST['physical_address'],
		    'phone  ' => $_POST['phone'],
		    'website' => $_POST['website'],
		    'email' => $_POST['email'],
		    'under_type' => 'company',
		    'under_into' => $_POST['under_into'],
		    'created_by' => $_SESSION['id']
		);

		$data = $this->security->xss_clean($data);

		$id = $newModel->insert($data);
		$this->db->trans_commit();
		$this->session->set_flashdata('msg', 'New Branch has been added successfully');
		redirect('administrator/branches');
	    }
	}
    }
    //-- update users info
    public function update($id) {
	$newModel = new Branches_model;
	$data = array();
	$data['page_title'] = 'Add Branch';
	$this->form_validation->set_rules('branch_name', 'Branch Name', 'required|xss_clean');
	$this->form_validation->set_rules('VAT_no', 'VAT NO.', 'required|xss_clean');
	$this->form_validation->set_rules('physical_address', 'Physical Address', 'required|xss_clean');
	$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean');
	$this->form_validation->set_rules('website', 'Web Site', 'required|xss_clean');
	$this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
	//$this->form_validation->set_rules('under_type', 'Under to', 'required|xss_clean');
	if ($this->form_validation->run() === FALSE) {
	    $data['branche'] = $newModel->getDetail($id);
	    $data['companies'] = $this->Common_model->selectTable('companies');
	    $data['main_content'] = $this->load->view('backend/Branches/edit', $data, TRUE);
	    $this->load->view('admin/index', $data);
	} else {
	    //   echo "kjdfhng;olsdf"; die;
	    if ($_POST) {
		$data = array(
		    'branch_name  ' => $_POST['branch_name'],
		    'VAT_no' => $_POST['VAT_no'],
		    'physical_address' => $_POST['physical_address'],
		    'phone  ' => $_POST['phone'],
		    'website' => $_POST['website'],
		    'email' => $_POST['email'],
		    'under_type' => 'company',
		    'under_into' => $_POST['under_into'],
			//    'created_by' => $_SESSION['id']
		);

		$data = $this->security->xss_clean($data);
		$newModel->update($data, $id);
		$this->db->trans_commit();
		$this->session->set_flashdata('msg', 'Branch has been updated successfully');
		redirect('administrator/branches');
	    }
	}
    }
    //-- delete user
    public function delete($id) {
	$newModel = new Branches_model;
	$satus['status']='2';
		$id = $newModel->update($satus, $id);
	$this->session->set_flashdata('msg', 'Branch has been deleted Successfully');
	 redirect('administrator/branches');
    }

    public function view($id) {
	$newModel = new Branches_model;
	$data = array();
	$data['page_title'] = 'All Branches';
	$branche = $newModel->getDetail($id);
	if (!empty($branche)) {
	    $data['branche'] = $branche;
	    $data['main_content'] = $this->load->view('backend/Branches/view', $data, TRUE);
	    $this->load->view('admin/index', $data);
	} else {
	    $this->session->set_flashdata('error_msg', 'No Record');
	    redirect('administrator/branches');
	}
    }
}
