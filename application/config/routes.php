<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/* company management */
$route['administrator/branches']='backend/Branches';
$route['administrator/branches/add'] = 'backend/Branches/add';
$route['administrator/branches/update/(:any)'] = 'backend/Branches/update/$1';
$route['administrator/branches/delete/(:any)'] = 'backend/Branches/delete/$1';
$route['administrator/branches/view/(:any)'] = 'backend/Branches/view/$1';

/* Groups */
$route['administrator/group']='backend/Group';
$route['administrator/group/add'] = 'backend/Group/add';
$route['administrator/group/update/(:any)'] = 'backend/Group/update/$1';
$route['administrator/group/delete/(:any)'] = 'backend/Group/delete/$1';
$route['administrator/group/view/(:any)'] = 'backend/Group/view/$1';


/* Bin */
$route['administrator/bin']='backend/Bin';
$route['administrator/bin/add'] = 'backend/Bin/add';
$route['administrator/bin/update/(:any)'] = 'backend/Bin/update/$1';
$route['administrator/bin/delete/(:any)'] = 'backend/Bin/delete/$1';
$route['administrator/bin/view/(:any)'] = 'backend/Bin/view/$1';

/* Categories */
$route['administrator/categories']='backend/Categories';
$route['administrator/categories/add'] = 'backend/Categories/add';
$route['administrator/categories/update/(:any)'] = 'backend/Categories/update/$1';
$route['administrator/categories/delete/(:any)'] = 'backend/Categories/delete/$1';
$route['administrator/categories/view/(:any)'] = 'backend/Categories/view/$1';



$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// companies tajinder 
$route['admin/companies']      = 'admin/Company';
$route['admin/add-company']    = 'admin/Company/add_company';
$route['admin/update-company/(:any)'] = 'admin/Company/edit_company/$1';
$route['admin/view-company/(:any)']   = 'admin/Company/view_company/$1';


/**********Dashboard*************/
$route['administrator/dashboard'] = 'admin/dashboard/index';
$route['admin/dashboard']         = 'administrator/dashboard';
