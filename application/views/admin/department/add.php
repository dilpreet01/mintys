<!-- Start Page Content -->

<div class="row">
    <div class="col-lg-12">


        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-plus"></i> &nbsp;Add New Department <a href="<?php echo base_url('admin/department') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-list"></i> List Departments </a>

            </div>
            <div class="panel-body table-responsive" id="add_department_div">

                <?php $error_msg = $this->session->flashdata('error_msg'); ?>
                <?php if (isset($error_msg)) : ?>
                    <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                <?php endif ?>


                <form method="post" id="add_department_form" action="<?php echo base_url('admin/department/add') ?>" class="form-horizontal" novalidate>
                    <div class="form-group">
                        <label class="col-md-12" for="example-text">Code</label>
                        <div class="col-sm-12">
                            <input type="text" name="code" id="code" class="form-control" required data-validation-required-message="Code is required">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-12" for="example-text">Description</label>
                        <div class="col-sm-12">
                            <input type="text" name="description" id="description" class="form-control" required data-validation-required-message="Description is required">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-12" for="example-text">Type</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="checkOuter">
                                <div class="form-check">
                                    <select class="" name="type">
                                        <option value="1" selected>
                                            <div class="label label-table label-success">Consumable</div>
                                        </option>
                                        <option value="2">
                                            <div class="label label-table label-danger">Non-Consumable</div>
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                    <hr>
                    <!-- CSRF token -->
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" id="save_department_button" class="btn btn-info btn-rounded btn-sm"> <i class="fa fa-plus"></i>&nbsp;&nbsp;Save</button>
                            <button type="button" id="cancel_department_button" class="btn btn-info btn-rounded btn-sm"> <i class="fa fa-plus"></i>&nbsp;&nbsp;Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<!-- End Page Content -->

<script type="text/javascript">
$('#cancel_department_button').on('click', function(e) {
    var form = document.getElementById('add_department_form');
    form.reset();
    $('#add_department_form').data('formValidation').resetForm(form);
});
</script>