<!-- Start Page Content -->

<div class="row">
    <div class="col-lg-12">


        <div class="panel panel-info">
            <div class="panel-heading"> <i class="fa fa-list"></i> All Departments
                <a href="<?php echo base_url('admin/department/new_department') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-plus"></i>&nbsp;New Department</a> &nbsp;
            </div>

            <div class="panel-body table-responsive">

                <?php $msg = $this->session->flashdata('msg'); ?>
                <?php if (isset($msg)) : ?>
                    <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                <?php endif ?>

                <?php $error_msg = $this->session->flashdata('error_msg'); ?>
                <?php if (isset($error_msg)) : ?>
                    <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                <?php endif ?>
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <?php foreach ($departments as $department) : ?>
                            <tr>
                                <td><?php echo $department['code']; ?></td>
                                <td><?php echo $department['description']; ?></td>
                                <td class="text-nowrap">

                                    <?php if ($this->session->userdata('role') == 'admin') : ?>

                                        <a href="<?php echo base_url('admin/department/edit/' . $department['id']) ?>"><button type="button" class="btn btn-info btn-circle btn-xs"><i class="fa fa-edit"></i></button></a>

                                        <a href="#" onClick="deleteConfirm('<?php echo $department['id']; ?>');" id="delete_department_anchor" data-toggle="tooltip" data-original-title="Delete"><button type="button" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-times"></i></button></a>
                                        
                                    <?php endif ?>

                                </td>
                            </tr>

                        <?php endforeach ?>

                    </tbody>

                </table>
            </div>


        </div>
    </div>
</div>

</div>

<!-- End Page Content -->

<script type="text/javascript">
    function deleteConfirm(id) {
        swal({
            title: "Are you sure?",
            text: "Are you sure you want to delete the item?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                var url = '<?php echo base_url('admin/department/delete/') ?>' + id;
                $.get(url, (req, res) => {
                    swal({
                        title: "Deleted!",
                        text: "Department has been deleted.",
                        type: "success"
                    }, () => {
                        location.reload(true);
                    });
                });
            }
        });
    }
</script>