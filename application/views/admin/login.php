<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo base_url(); ?>assets/minty-logo.jpg" type="image/x-icon" />
	<title><?php echo $page_title; ?> - Mintys</title>
	<!-- Bootstrap Core CSS -->
	<link href="<?php echo base_url(); ?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
	<!-- animation CSS -->
	<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	<!-- color CSS -->
	<link href="<?php echo base_url(); ?>assets/css/colors/megna.css" id="theme" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div class="container">
		<section id="wrapper" class="login-register">
			<div class="white-box login-box applicantPersonalblock">

				<div align="center">
					<div class="site-logo">

						<img width="250px" height="auto" src="<?php echo base_url(); ?>assets/minty-logo.jpg">
					</div>
					<!--
                  <h5 style="color:white">Admin Login</h5>
-->
					<br>
					<br>
					<div align="center">
						<?php if (isset($page) && $page == "logout") : ?>
							<div class="alert alert-success hide_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> Logout Successfully &nbsp;
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
							</div>
						<?php endif ?>
						<?php  if($this->session->flashdata('error')) {   ?>
							<div class="alert alert-danger hide_msg pull" style="width: 100%"> <i class="fa fa-warning"></i> <?php echo $this->session->flashdata('error');  ?> &nbsp;
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
							</div>
						<?php } ?>	
					</div>
				</div>
				<form class="form-vertical form-material" id="login-form" action="<?php echo base_url('auth/log'); ?>" method="post">

					<div class="form-group">

						<div class="col-xs-12">
							<input class="form-control __loginPage" type="email" name="user_name" value="" required="" placeholder="Email Address" style="width:100%">
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control __loginPage" type="password" name="password" value="" required="" placeholder="Password" style="width:100%">
						</div>
					</div>


					<!-- CSRF token -->
					<input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
					<button class="btn btn-info style1 btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" style="width:100%; color:white">
						Login
					</button>
					<div class="col-xs-4 forgot_pswd_div">
						<a href="<?php echo base_url('administrator/forgot-password'); ?>" id="forgot_pswd">Forgot password?</a>
					</div>
					<div align="center"><img id="install_progress" src="<?php echo base_url() ?>assets/images/loading.gif" style="width: 22px;
					display: none;" /></div>
				</form>
			</div>
		</section>
	</div>
	<!-- </div> -->
	<!-- </div> -->



	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="<?php echo base_url(); ?>assets/bootstrap/dist/js/tether.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>

	<!--Custom JavaScript -->
	<script src="<?php echo base_url() ?>assets/js/custom.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/custom.js"></script>



	<!-- Menu Plugin JavaScript -->
	<script src="<?php echo base_url(); ?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
	<link href="<?php echo base_url(); ?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">

	<!-- auto hide message div-->
	<script type="text/javascript">
		$(document).ready(function() {
			$('.hide_msg').delay(10000).slideUp();
		});
	</script>

	<!--slimscroll JavaScript -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
	<!--Wave Effects -->
	<!-- <script src="<?php echo base_url(); ?>assets/js/waves.js"></script> -->
	<!-- Custom Theme JavaScript -->
	<script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>
	<!--Style Switcher -->
	<script src="<?php echo base_url(); ?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

	<script>
		$('form').submit(function(e) {
			$('#install_progress').show();
			$('#modal_1').show();
			$('.btn').val('Login...');
			$('form').submit();
			e.preventDefault();
		});
	</script>

</body>

</html>
