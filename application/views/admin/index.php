<?php include 'layout/css.php'; ?>

<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="icon-grid"></i></a>
            <div class="top-left-part"><a class="logo" href="<?php echo base_url('admin/dashboard/') ?>"><b><img src="<?php echo base_url(); ?>assets/minty-logo.jpg" height="auto" width="50px" alt="Codeig" /></b><span class="hidden-xs">Mintys</span></a></div>
            <ul class="nav navbar-top-links navbar-right pull-right">
				<li class="dropdown">
					<a class="dropdUntitled Folderown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="<?php echo base_url(); ?>assets/images/admin.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo $this->session->userdata('name'); ?></b> </a>
					<ul class="dropdown-menu dropdown-user animated flipInY">
						<li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
						<li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
						<li><a href="<?php echo base_url('auth/logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
					</ul>
					<!-- /.dropdown-user -->
				</li>
			</ul>
        </div>
    </nav>
    <!-- Left navbar-header -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse slimscrollsidebar">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                    <!-- input-group -->
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search<?php echo base_url(); ?>assets."> <span class="input-group-btn">
                            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                        </span> </div>
                    <!-- /input-group -->
                </li>
<!--
                <li class="user-pro">
                    <a href="#" class="waves-effect"><img src="<?php echo base_url(); ?>assets/images/admin.jpg" alt="user-img" class="img-circle"> <span class="hide-menu"><?php echo $this->session->userdata('name'); ?><span class="fa arrow"></span></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
                        <li><a href="<?php echo base_url('auth/logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                </li>
-->
                <li> <a href="<?php echo base_url('admin/dashboard') ?>" class="waves-effect"><i class="ti-dashboard p-r-10"></i> <span class="hide-menu">Dashboard</span></a> </li>
               
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="icon-user p-r-10"></i> <span class="hide-menu"> Departments <span class="fa arrow"></span><span class="label label-rouded label-danger pull-right">3</span></span></a>
                    <ul class="nav nav-second-level">
                        <?php if ($this->session->userdata('role') == 'admin') : ?>
                            <li> <a href="<?php echo base_url('admin/department/new_department') ?>"><i class="fa fa-plus p-r-10"></i><span class="hide-menu">New Department</span></a></li>
                        <?php endif ?>
                        <li><a href="<?php echo base_url('admin/department') ?>"><i class="fa fa-list p-r-10"></i><span class="hide-menu">All Departments</span></a></li>
                    </ul>
                </li>
                
                <li> <a href="javascript:void(0);" class="waves-effect"><i class="icon-user p-r-10"></i> <span class="hide-menu"> Comapnies <span class="fa arrow"></span><span class="label label-rouded label-danger pull-right">3</span></span></a>
                    <ul class="nav nav-second-level">
                        <?php if ($this->session->userdata('role') == 'admin') : ?>
                            <li> <a href="<?php echo base_url('admin/add-company') ?>"><i class="fa fa-plus p-r-10"></i><span class="hide-menu">New Company</span></a></li>
                        <?php endif ?>
                        <li><a href="<?php echo base_url('admin/companies') ?>"><i class="fa fa-list p-r-10"></i><span class="hide-menu">All Companies</span></a></li>
                    </ul>
                </li>
				
				<li> <a href="#" class="waves-effect"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Management<span class="fa arrow"></span> <span class="label label-rouded label-info pull-right">25</span> </span></a>
						    <ul class="nav nav-second-level">
							<li><a href="<?php echo base_url('administrator/branches') ?>">Branches</a></li>
							<li> <a href="<?php echo base_url('administrator/group') ?>">Groups</a></li>
							<li> <a href="<?php echo base_url('administrator/bin') ?>">Bin</a></li>
							<li> <a href="<?php echo base_url('administrator/categories') ?>">Categories</a></li>
						    </ul>
						</li>

               

                <li><a href="<?php echo base_url('auth/logout') ?>" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
            </ul>
            </div>

        </div>
    </div>
    <!-- Left navbar-header end -->


    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Mintys Software System</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>admin/dashboard/">Home</a></li>
                        <li class="active"> <?php echo $page_title; ?></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!--  row    ->
               <?php echo $main_content; ?>
                <!-- /.row -->

        </div>
        <!-- /.container-fluid -->
        <?php include 'layout/footer.php'; ?>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<?php include 'layout/js.php'; ?>
