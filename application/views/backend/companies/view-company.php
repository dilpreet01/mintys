<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?php echo $page_title; ?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>administrator/dashboard">Home</a></li>

            <li><a href="<?php echo base_url(); ?>admin/add-company">All Companies</a></li>

            <li class="active"> <?php echo $page_title; ?></li>
        </ol>
    </div>
</div>
<!-- .row -->
<div class="row">
    <div class="col-lg-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Company Detail </h3>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Company Name</td>
                            <td><?= $company['0']['company_name']; ?></td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td><?= $company['0']['location']; ?></td>
                        </tr>
                         <tr>
                            <td>Created At</td>
                            <td><?= date('Y-m-d', strtotime($company['0']['created_at'])); ?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
