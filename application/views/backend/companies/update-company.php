    <!-- Start Page Content -->
    <div class="row">
        <div class="col-lg-12"> 
           <div class="panel panel-info">
                <div class="panel-heading"> 
                     <i></i> &nbsp;Update Company <a href="<?php echo base_url('admin/companies') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-list"></i> All Companies </a>
                </div>
                <div class="panel-body table-responsive">
                 <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>
            <?php foreach ($result as $company):?>
                <form method="post" action="<?php echo base_url('admin/update-company/'.$company['id']) ?>" class="form-horizontal" novalidate>
                    <div class="form-group">
                    <label class="col-md-12" for="example-text">Company Name</label>
                    <div class="col-sm-12">
                            <input type="text" name="company_name" class="form-control" value="<?php echo $company['company_name']; ?>">
                    </div>
                     <?php echo form_error('company_name', '<div class="help-error">', '</div>'); ?>
                    </div>
                    <div class="form-group">
                    <label class="col-md-12" for="example-text">Location</label>
                    <div class="col-sm-12">
                            <input type="text" name="location" class="form-control" value="<?php echo $company['location']; ?>" >
                    </div>
                    <?php echo form_error('location', '<div class="help-error">', '</div>'); ?>
                    </div>                    
                            <!-- CSRF token -->
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />

                        <input type="hidden" name="company_id" value="<?php echo $company['id'];;?>" />

                        <div class="form-group w-100">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info btn-rounded btn-sm" name="submit"></i>&nbsp;Submit</button>
                            <button type="button" class="btn btn-info btn-rounded btn-sm"></i>&nbsp;Cancel</button>
                        </div>
                    </div> 
                <?php endforeach ?>
                </form>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Content -->