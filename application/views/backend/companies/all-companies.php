<!-- Start Page Content -->
    <div class="row">
        <div class="col-lg-12">  
           <div class="panel panel-info">
                <div class="panel-heading"> <i class="fa fa-list"></i> All Companies
                    <a href="<?php echo base_url('admin/add-company') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-plus"></i>&nbsp;Add Company</a> &nbsp;
                </div>
                <div class="panel-body table-responsive">
                 <?php $msg = $this->session->flashdata('success_msg'); ?>
            <?php if (isset($msg)): ?>
                <div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>
            <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>
            <?php $delete_msg = $this->session->flashdata('delete'); ?>
            <?php if (isset($delete_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $delete_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>
                            <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Company Name</th>
                                    <th>Location</th>
                                    <th>Joining Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>                            
                            <tbody>
                            <?php $count = 1; foreach ($companies as $res): ?>
                                
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $res['company_name']; ?></td>
                                    <td><?php echo $res['location']; ?></td>
                                    <td><?php echo date('Y-m-d', strtotime($res['created_at'])); ?></td>
                                    <td class="text-nowrap">

                                        <a href="<?= base_url('admin/view-company/' . $res['id']) ?>" data-toggle="tooltip" data-original-title="View">
                                        <button type="button" class="btn btn-primary btn-circle btn-xs">
                                        <i class="fa fa-info"></i></button>
                                        </a>

                                        <a href="<?php echo base_url('admin/update-company/'.$res['id']) ?>"><button type="button" class="btn btn-info btn-circle btn-xs"><i class="fa fa-edit"></i></button></a>
                                                                              
                                        <a href="<?php echo base_url('admin/Company/delete/'.$res['id']) ?>" onclick="delete_company();" data-toggle="tooltip" data-original-title="Delete"><button type="button" class="btn btn-danger btn-circle btn-xs"><i class="fa fa-times"></i></button></a>
                                    </td>
                                </tr>
                            <?php $count++; endforeach ?>
                         </tbody>
                     </table>
                </div>  
            </div>
        </div>
    </div>
 </div>
<!-- End Page Content -->