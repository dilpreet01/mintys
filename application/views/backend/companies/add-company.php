
    <!-- Start Page Content -->

    <div class="row">
        <div class="col-lg-12">

            
           <div class="panel panel-info">
                <div class="panel-heading"> 
                     <i class="fa fa-plus"></i> &nbsp;Add New User <a href="<?php echo base_url('admin/user/all_user_list') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-list"></i> List Users </a>

                </div>
                <div class="panel-body table-responsive">
                 <?php $error_msg = $this->session->flashdata('error_msg'); ?>
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            <?php endif ?>
                <form method="post" action="<?php echo base_url('admin/add-company') ?>" class="form-horizontal" novalidate>
                    <div class="form-group">
                    <label class="col-md-12" for="example-text">Comapny Name</label>
                    <div class="col-sm-12">
                            <input type="text" name="company_name" class="form-control">
                    </div>
                     <?php echo form_error('company_name', '<div class="help-error">', '</div>'); ?>
                    </div>
                    <div class="form-group">
                    <label class="col-md-12" for="example-text">Location</label>
                    <div class="col-sm-12">
                            <input type="text" name="location" class="form-control">
                    </div>
                    <?php echo form_error('location', '<div class="help-error">', '</div>'); ?>
                    </div>                    
                            <!-- CSRF token -->
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                        <div class="form-group w-100">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info btn-rounded btn-sm" name="submit"></i>&nbsp;Submit</button>
                            <button type="button" class="btn btn-info btn-rounded btn-sm"></i>&nbsp;Cancel</button>
                        </div>
                    </div> 
                </form>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Content -->