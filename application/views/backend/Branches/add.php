
<!-- Start Page Content -->

<div class="row">
    <div class="col-lg-12">


	<div class="panel panel-info">
	    <div class="panel-heading"> 
		<i class="fa fa-plus"></i> &nbsp;Add New Branch <a href="<?php echo base_url('administrator/branches') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-list"></i> List Branches </a>

	    </div>
	    <div class="panel-body table-responsive">

		<?php $error_msg = $this->session->flashdata('error_msg'); ?>
		<?php if (isset($error_msg)) : ?>
    		<div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
    		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">�</span> </button>
    		</div>
		<?php endif ?>


		<form method="post" id="branchVelidation" name="branchVelidation" action="<?php echo base_url('administrator/branches/add') ?>" class="form-horizontal" novalidate>
		    <div class="form-group">
			<label class="col-md-12" for="example-text">Branch Name<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <input type="text" name="branch_name" id="import_move_number" class="form-control" value="<?php echo set_value('branch_name') ?>">
			    <?php echo form_error('branch_name', '<div class="help-error">', '</div>'); ?>

			</div>
		    </div>

		    <div class="form-group">
			<label class="col-md-12" for="example-text">VAT NO<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <input type="text" name="VAT_no" id="skva_file_ref" class="form-control" value="<?php echo set_value('VAT_no') ?>" autocomplete="off">
			    <?php echo form_error('VAT_no', '<div class="help-error">', '</div>'); ?>

			</div>
		    </div>


		    <div class="form-group">
			<label class="col-md-12" for="example-text">Physical Address<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <input type="text" name="physical_address" class="form-control" value="<?php echo set_value('physical_address') ?>" autocomplete="off">
			    <?php echo form_error('physical_address', '<div class="help-error">', '</div>'); ?>

			</div>
		    </div>




		    <div class="form-group">
			<label class="col-md-12" for="example-text">Phone<sup class="astrick">*</sup></label>
			<div class="col-sm-12">

			    <input type="text" class="form-control"  name="phone"  placeholder="" value="<?php echo set_value('phone') ?>" autocomplete="off">
			    <?php echo form_error('phone', '<div class="help-error">', '</div>'); ?>

			</div>
		    </div>
		    <div class="form-group">
			<label class="col-md-12" for="example-text">Under to<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <select class="form-control custom-select" name="under_into" id="type" data-id="<?php echo set_value('under_into') ?>">

				<option value="">select company</option>
				<?php
				if (!empty($companies)) {
				    foreach ($companies as $key => $company) {
					?>
					<option value="<?php echo $company->id ?>"><?= $company->company_name ?></option>
					<?php
				    }
				}
				?>

			    </select>
			    <?php echo form_error('under_into', '<div class="help-error">', '</div>'); ?>
			</div>
		    </div>

		    <div class="form-group">
			<label class="col-md-12" for="example-text">Web Site<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <input type="text" class="form-control"  name="website"  value="" placeholder="" value="<?php echo set_value('website') ?>" autocomplete="off">
			    <?php echo form_error('website', '<div class="help-error">', '</div>'); ?>

			</div>
		    </div>



		    <div class="form-group">
			<label class="col-md-12" for="example-text">Email<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <input type="email" name="email" id="last_inspection_date"  class="form-control startDate" value="<?php echo set_value('email') ?>" autocomplete="off">
			    <?php echo form_error('email', '<div class="help-error">', '</div>'); ?>

			</div>
		    </div>


		    <hr>   
		    <!-- CSRF token -->
		    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
		    <div class="form-group">
			<div class="col-sm-offset-3 col-sm-5">
			    <button type="submit" class="btn btn-info btn-rounded btn-sm"> <i class="fa fa-plus"></i>&nbsp;&nbsp;Save</button>
			</div>
		    </div>
		</form>
	    </div>


	</div>
    </div>
</div>
</div>

<!-- End Page Content -->