
<!-- Start Page Content -->

<div class="row">
    <div class="col-lg-12">


	<div class="panel panel-info">
	    <div class="panel-heading"> 
		<i class="fa fa-plus"></i> &nbsp;Add New Group <a href="<?php echo base_url('administrator/group') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-list"></i> List Groups </a>

	    </div>
	    <div class="panel-body table-responsive">
		<?php $error_msg = $this->session->flashdata('error_msg'); ?>
		<?php if (isset($error_msg)) : ?>
    		<div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
    		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">�</span> </button>
    		</div>
		<?php endif ?>
		<form method="post" id="branchVelidation" name="branchVelidation" action="<?php echo base_url('administrator/group/add') ?>" class="form-horizontal" novalidate>
		    <div class="form-group">
			<label class="col-md-12" for="example-text">Group Code<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <input type="text" name="group_code" id="import_move_number" class="form-control" value="<?php echo set_value('group_code') ?>">
			    <?php echo form_error('group_code', '<div class="help-error">', '</div>'); ?>
			</div>
		    </div>
		    <div class="form-group">
			<label class="col-md-12" for="example-text">Description<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <input type="text" name="description" id="skva_file_ref" class="form-control" value="<?php echo set_value('description') ?>" autocomplete="off">
			    <?php echo form_error('description', '<div class="help-error">', '</div>'); ?>
			</div>
		    </div>		  
		    <hr>   
		    <!-- CSRF token -->
		    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
		    <div class="form-group">
			<div class="col-sm-offset-3 col-sm-5">
			    <button type="submit" class="btn btn-info btn-rounded btn-sm"> <i class="fa fa-plus"></i>&nbsp;&nbsp;Save</button>
			</div>
		    </div>
		</form>
	    </div>


	</div>
    </div>
</div>
</div>

<!-- End Page Content -->