
<!-- Start Page Content -->

<div class="row">
    <div class="col-lg-12">


	<div class="panel panel-info">
	    <div class="panel-heading"> 
		<i class="fa fa-plus"></i> &nbsp;Add Update Category <a href="<?php echo base_url('administrator/categories') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-list"></i> List Categories </a>

	    </div>
	    <div class="panel-body table-responsive">
		<?php $error_msg = $this->session->flashdata('error_msg'); ?>
		<?php if (isset($error_msg)) : ?>
    		<div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
    		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">�</span> </button>
    		</div>
		<?php endif ?>
		<form method="post" id="branchVelidation" name="branchVelidation" action="<?php echo base_url('administrator/categories/update/') ?><?php echo $category->id;?>" class="form-horizontal" novalidate>
		    <div class="form-group">
			<label class="col-md-12" for="example-text">Category Code<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <input type="text" name="category_code" class="form-control" value="<?php echo $category->category_code ;?>">
			    <?php echo form_error('category_code', '<div class="help-error">', '</div>'); ?>
			</div>
		    </div>
		    <div class="form-group">
			<label class="col-md-12" for="example-text">Description<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
			    <input type="text" name="description"  class="form-control" value="<?php echo $category->description ;?>" autocomplete="off">
			    <?php echo form_error('description', '<div class="help-error">', '</div>'); ?>
			</div>
		    </div>
		     <div class="form-group">
			<label class="col-md-12" for="example-text">Status<sup class="astrick">*</sup></label>
			<div class="col-sm-12">
				<select name="status" class="form-control" id="status" required data-id="status">			   
				   <?php if($category->status=='1'){?>
				    <option value="1">Active</option>
				    <?php }else{?>
				    <option value="0">Inactive</option>
				    <?php } ?>
				       <?php if($category->status!='1'){?>
				    <option value="1">Active</option>
				    <?php }else{?>
				    <option value="0">Inactive</option>
				    <?php } ?>
				</select>

				<?php echo form_error('status', '<div class="help-error">', '</div>'); ?>
			  </div>
		    </div>
		    <hr>   
		    <!-- CSRF token -->
		    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
		    <div class="form-group">
			<div class="col-sm-offset-3 col-sm-5">
			    <button type="submit" class="btn btn-info btn-rounded btn-sm"> <i class="fa fa-plus"></i>&nbsp;&nbsp;Save</button>
			</div>
		    </div>
		</form>
	    </div>


	</div>
    </div>
</div>
</div>

<!-- End Page Content -->