<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">View Isotank</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>administrator/dashboard">Home</a></li>
            <li><a href="<?php echo base_url(); ?>administrator/isotanks">Isotanks List</a></li>
            <li class="active"> <?php echo $title; ?></li>
        </ol>
    </div>
</div>
<!-- .row -->
<div class="row">
    <div class="col-lg-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Isotank Detail </h3>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
			<?php if (!empty($branche)) { ?>
                            <tr>
                                <td>Branch Name</td>
                                <td><?php echo $branche->branch_name; ?> </td>
                            </tr>
    			<tr>
                                <td>VAT NO</td>
                                <td><?php echo $branche->VAT_no; ?> </td>
                            </tr>
    			<tr>
                                <td>Company Name</td>
                                <td><?php echo company_name($branche->under_into);?> </td>
                            </tr>
    			<tr>
                                <td>Physical Address</td>
                                <td><?php echo $branche->physical_address; ?> </td>
                            </tr>   <tr>
                                <td>Phone</td>
                                <td><?php echo $branche->phone; ?> </td>
                            </tr>   <tr>
                                <td>Web Site</td>
                                <td><?php echo $branche->website; ?> </td>
                            </tr>   <tr>
                                <td>Email</td>
                                <td><?php echo $branche->email; ?> </td>
                            </tr>   
			<?php } ?>




                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /.row -->
