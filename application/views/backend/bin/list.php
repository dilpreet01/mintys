
<!-- Start Page Content -->

<div class="row">
    <div class="col-lg-12">


	<div class="panel panel-info">
	    <div class="panel-heading"> <i class="fa fa-list"></i>All Bins


		<a href="<?php echo base_url('administrator/bin/add') ?>" class="btn btn-info btn-sm pull-right"><i class="fa fa-plus"></i>&nbsp;New Bin</a>


	    </div>

	    <div class="panel-body table-responsive">

		<?php $msg = $this->session->flashdata('msg'); ?>
		<?php if (isset($msg)): ?>
    		<div class="alert alert-success delete_msg pull" style="width: 100%"> <i class="fa fa-check-circle"></i> <?php echo $msg; ?> &nbsp;
    		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    		</div>
		<?php endif ?>

		<?php $error_msg = $this->session->flashdata('error_msg'); ?>
		<?php if (isset($error_msg)): ?>
    		<div class="alert alert-danger delete_msg pull" style="width: 100%"> <i class="fa fa-times"></i> <?php echo $error_msg; ?> &nbsp;
    		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    		</div>
		<?php endif ?>
		<table id="example23" class="display nowrap" cellspacing="0" width="100%">
		    <thead>
			<tr>
			    <th>Bin code</th>
			    <th>Description</th>			  
			    <th>Status</th>
			    <th>Joining Date</th>
			    <th>Action</th>
			</tr>
		    </thead>


		    <tbody>



			<?php
			if (!empty($bins)) {
			    foreach ($bins as $bin) {
				?>
				<tr>
				    <td><?php echo $bin->bin_code; ?></td>
				    <td><?php echo $bin->description; ?></td>				  				  
				    <td>
					<?php if ($bin->status == '0') { ?>
	    				<div class="label label-table label-danger">Inactive</div>
					<?php }if ($bin->status == '1') { ?>
	    				<div class="label label-table label-success">Active</div>
					<?php } ?>
				    </td>
				    <td><?php echo $bin->created_at; ?></td>
				    <td class="text-nowrap">
<!--
					<a href="<?= base_url('administrator/bin/view/' . $bin->id) ?>" data-toggle="tooltip" data-original-title="View">
					    <button type="button" class="btn btn-primary btn-circle btn-xs">
						<i class="fa fa-info"></i>
					    </button>
					</a>-->

					<a href="<?= base_url('administrator/bin/update/' . $bin->id) ?>">
					    <button type="button" class="btn btn-info btn-circle btn-xs">
						<i class="fa fa-edit"></i>
					    </button>
					</a>

					
	    				<a href="<?= base_url('administrator/bin/delete/' . $bin->id) ?>" onClick="return confirm('Are you sure want to delete?');" data-toggle="tooltip" data-original-title="Delete">
	    				    <button type="button" class="btn btn-danger btn-circle btn-xs">
	    					<i class="fa fa-times"></i>
	    				    </button>
	    				</a>
			
				    </td>
				</tr>
				<?php
			    }
			}
			?>




		    </tbody>


		</table>
	    </div>


	</div>
    </div>
</div>

</div>


<!-- End Page Content -->