<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* * * Tank_model Model Class
 *
 * @package Model
 * @subpackage Admin
 *
 * * */


class Group_model extends CI_Model {

    /**
     * @var string table name
     */
    public $table = 'groups';

    /**
     * @ Function Name		: __construct
     * @ Function Params	:
     * @ Function Purpose 	: Auto loading function, specify all the parameter which requires to be used global when this class loads
     * @ Function Returns	:
     */
    public function __construct() {
	parent::__construct();
    }

    /**
     * @ Function Name      : insert
     * @ Function Params    : $data
     * @ Function Purpose   : inserting import Invoice data into database
     * @ Function Returns   : last detail id
     */
    public function insert($data) {	
	$this->db->insert($this->table, $data);
	return $this->db->insert_id();
    }

    /**
     * @ Function Name      : insert
     * @ Function Params    : $data
     * @ Function Purpose   : inserting import Invoice data into database
     * @ Function Returns   : last detail id
     */
    public function insertTable($data, $table) {


	$this->db->insert($table, $data);
	return $this->db->insert_id();
    }

    /**
     * @ Function Name		: getDetails
     * @ Function Params	:
     * @ Function Purpose 	: get specfic data of import Invoice
     * @ Function Returns	: All details
     */
    public function getDetailsWithWhere($where) {
	$this->db->where('status !=', '2');
	$this->db->where($where);
	$query = $this->db->get($this->table);
	return $query->result();
    }

    /**
     * @ Function Name		: getDetails
     * @ Function Params	:
     * @ Function Purpose 	: get specfic data of import Invoice
     * @ Function Returns	: All details
     */
    public function getDetailsWithWhereTable($where, $table) {
	$this->db->where($where);
	$query = $this->db->get($table);
	return $query->result();
    }

    /**
     * @ Function Name		: getDetails
     * @ Function Params	:
     * @ Function Purpose 	: get specfic data of import Invoice
     * @ Function Returns	: All details
     */
    public function getRow($where) {
	$this->db->where('status !=', '2');
	$this->db->where($where);
	$query = $this->db->get($this->table);
	return $query->row();
    }

  
     /**
     * @ Function Name		: getDetails
     * @ Function Params	:
     * @ Function Purpose 	: get specfic data of import Invoice
     */
    public function getRowcheck($where) {
	$this->db->where('status !=', '2');
	$this->db->where($where);
	$query = $this->db->get($this->table);
	return $query->row();
    }

    /**
     * @ Function Name		: update
     * @ Function Params	: $data, $id
     * @ Function Purpose 	: updating import Invoice data into database
     * @ Function Returns	:
     */
    function update($data, $id) {
	$this->db->where('id', $id);
	$this->db->update($this->table, $data);
	return;
    }
    
    /* * @ Function Name		: update
     * @ Function Params	: $data, $id
     * @ Function Purpose 	: updating import Invoice data into database
     * @ Function Returns	:
     */
    function updateWhere($data, $where) {
	$this->db->where($where);
	$this->db->update($this->table, $data);
	return;
    }

  
    /**
     * @ Function Name		: getDetails
     * @ Function Params	:
     * @ Function Purpose 	: get not specfic data of import Invoice
     * @ Function Returns	: All details
     */
    public function getDetailsNotWhere() {
	$this->db->where('status !=','2');
	$query = $this->db->get($this->table);
	return $query->result();
    }

    /**
     * @ Function Name		: getDetail
     * @ Function Params	: $id
     * @ Function Purpose 	: get specfic data of import Invoice
     * @ Function Returns	: single detail
     */
    public function getDetail($id) {
	$this->db->where('status !=', '2');
	$this->db->where("id", $id);
	$query = $this->db->get($this->table);
	return $query->row();
    }

}
