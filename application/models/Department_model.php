<?php
defined('BASEPATH') or exit('No direct script access allowed!');

/**
 * Department Model class
 * 
 * This model class is used to perform department related
 * database queries by admin.
 * 
 * @access public
 * @package Model
 * @subpackage Model
 */
class Department_model extends CI_Model
{
    /**
     * Constructor function
     * 
     * This function is used for initialization.
     * 
     * @access public
     * @param void
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 
     */
    public function get_all_departments()
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('deleted_at IS NULL');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        $query = $query->result_array();
        return $query;
    }

    public function get_department_total()
    {
        return $this->db->count_all_results('department');
    }

    public function check_department($code)
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('code', $code);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function insert($data, $table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * 
     */
    public function get_department_details($id)
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $query = $query->row();
        return $query;
    }

    public function update($action, $id, $table)
    {
        $this->db->where('id', $id);
        $this->db->update($table, $action);
        return;
    }

    /**
     * Delete function
     * 
     * This function is called to delete a department.
     * 
     * @access public
     * @param mixed $id
     * @return void
     */
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->set('deleted_at', current_datetime());
        $this->db->update('department');
        return;
    }
}
