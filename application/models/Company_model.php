<?php
class Company_model extends CI_Model {

    public function all_companies(){
        $qry = $this->db->select('*')
                 ->from('companies')
                 ->get();
       if($qry) {
            return $res = $qry->result_array();
         } else {
            return false;
         }
    }


    public function add_company($data){

        $this->db->insert('companies', $data);

        if ($this->db->affected_rows()) {
                return true;
           } else {
                return false;
           }
        
    }

    public function edit_company($id){

        $qry = $this->db->select('*')
                 ->from('companies')
                 ->where('id', $id)
                 ->get();
       if($qry) {
            return $res = $qry->result_array();
         } else {
            return false;
         }
    }

    public function view_company($id){

        $qry = $this->db->select('*')
                 ->from('companies')
                 ->where('id', $id)
                 ->get();
       if($qry) {
            return $res = $qry->result_array();
         } else {
            return false;
         }
    }



    public function update_company($data, $id){
 
        $qry = $this->db->where('id', $id)
                 ->update('companies', $data);

        if ($this->db->affected_rows()) {
                return true;
           } else {
                return false;
           }

    }



    public function delete($id){
         $this->db->delete('companies', array('id' => $id));
           return;
    }
}