$(function () {
    $('#add_department_div').wizard({
        onInit: function () {
            $('#add_department_form').formValidation({
                framework: 'bootstrap',
                fields: {
                    code: {
                        validators: {
                            notEmpty: {
                                message: 'The code is required'
                            },
                            stringLength: {
                                min: 6,
                                max: 30,
                                message: 'The code must be more than 6 and less than 30 characters long'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: 'The code can only consist of alphabetical, number, dot and underscore'
                            }
                        }
                    },
                    description: {
                        validators: {
                            notEmpty: {
                                message: 'The description is required'
                            },
                            // emailAddress: {
                            //     message: 'The input is not a valid email address'
                            // }
                        }
                    },
                    added_by: {
                        validators: {
                            notEmpty: {
                                message: 'The added by is required'
                            },
                            // different: {
                            //     field: 'username',
                            //     message: 'The password cannot be the same as username'
                            // }
                        }
                    }
                }
            });
        },
        validator: function () {
            var fv = $('#add_department_form').data('formValidation');

            var $this = $(this);

            // Validate the container
            fv.validateContainer($this);

            var isValidStep = fv.isValidContainer($this);
            if (isValidStep === false || isValidStep === null) {
                return false;
            }

            return true;
        },
        onFinish: function () {
            $('#add_department_form').submit();
            swal("Message Finish!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");
        }
    });
});