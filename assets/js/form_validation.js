$(document).ready(function () {
    $.validator.addMethod("alpha", function (value, element) {
	return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    });
});


//$(function () {
//
//    $("form[name='JobCategoryForm']").validate({
//	rules: {
//	    title: {
//		required: true,
//		alpha: true,
//
//	    },
//
//	}, messages: {
//	    title: {
//		alpha: "Only alphabets are allowed."
//	    }
//	},
//
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});


$(function () {
    $("#branchVelidation").validate({
	rules: {
	    branch_name: {
		required: true,
		
	    },
	    email: {
		required: true,
		email: true
	    },
	    phone: {
		required: true,
		number: true,
		minlength: 10,
		maxlength: 15,

	    },
	    VAT_no: {
		required: true,
	    },
	     under_into: {
		required: true
	    },
	    physical_address: {
		required: true
	    },
	    website: {
		required: true,

	    },
	},
	messages: {
	    branch_name: {
		alpha: "Only alphabets are allowed."
	    },
	     email: {
		alpha: "Use only email"
	    },
	    
	},
	submitHandler: function (form) {
	    form.submit();
	}
    });
});

//
//
//$(function () {
//    $("form[name='edit_staff_password']").validate({
//	rules: {
//	    staff_email: {
//		required: true,
//		email: true
//	    },
//	    staff_password: {
//		required: true,
//	    },
//	},
//	//~ messages: {
//
//	//~ },
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
//
//
//$(function () {
//    $("form[name='settingForm']").validate({
//	rules: {
//	    name: "required",
//	    value: "required",
//	},
//	//~ messages: {
//
//	//~ },
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
///***************** Export Isotank Validation ****************/
//
//$(function () {
//    $("form[name='add_export_istotank_form']").validate({
//	rules: {
//	    import_stolt: {
//		required: true,
//		digits: true,
//	    },
//	    depot_id: "required",
//	    tank_number: "required",
//	    insp_cert: "required",
//	    skva_number: "required",
//	    //export: "required",
//	    client_id: "required",
//	    cref_number: "required",
//	    booking_number: "required",
//	    bill_number: "required",
//	    return_vessel: "required",
//	    ship: "required",
//	    etd: "required",
//	    eta: "required",
//	    as_four: "required",
//	    destination: "required",
//	    type: "required",
//	    tank_status: "required",
//	    initial_inspection_date: "required",
//	    last_inspection_date: "required",
//	    next_inspection_date: "required",
//	    confirmation_loading_date: "required",
//	    shipment_loading_date: "required",
//	    shipping_line: "required",
//	    port_of_loading: "required",
//	    port_of_discharge: "required",
//	},
//	//~ messages: {
//
//	//~ },
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
///***************** End Export Isotank Validation ****************/
//
//
///***************** Monthly activity Validation ****************/
//
//$(function () {
//    $("form[name='add_monthly_activity']").validate({
//	rules: {
//	    tank_number: {
//		required: true,
//		remote: {
//		    url: site_url + "checkisotanknumber",
//
//		    data: {
//			tank_number: function () {
//			    var val = $('#isotankNumber').val();
//			    console.log("isotank" + val);
//			    return val;
//			}
//		    }
//		}
//	    },
//	    status: "required",
//	    date_in: "required",
//	    eir_ref: "required",
//	    //    d_number: {
//	    // required: true,
//	    // digits: true,
//	    //    },
//	    storage_from: "required",
//	    storage_to: "required",
//	    days_depot: {
//		required: true,
//		digits: true,
//	    },
//	    lift_off: {
//		required: true,
//		digits: true,
//	    },
//	    lift_on: {
//		required: true,
//		digits: true,
//	    },
//	    transport_in: {
//		required: true,
//		digits: true,
//	    },
//	    invoice_period: {
//	    	required: true,
//	    	//digits: true
//	    },
//	    stolt:{
//	    	required:true,
//	    	number:true,
//	    },
//	    owned_lease:{
//	    	required:true,
//	    	number:true
//	    } ,
//	    
//	    unit_usd: {
//	    	required: true,
//	    	number: true
//	    },
//	    //    total_usd: {
//	    // required: true,
//	    // digits: true
//	    //    },
//	},
//	messages: {
//	    tank_number: {
//		remote: $.validator.format("{0} is already taken."),
//	    }
//
//	},
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
///***************** End Monthly activity Validation ****************/
//
//
//function deleteRow(clicked_id) {
//    $('#' + clicked_id).parent('.bannerDiv').parent('.fieldArea').remove();
//
//}
//
//$(document).ready(function () {
//    var i = bannerCounter;
//    $("#addBannerRow").on("click", function () {
//	$('#bannerArea').append('<div class="col-md-12 fieldArea"><div class="col-md-4 bannerDiv"> <input type="text" name="banner[title][' + i + ']" class="form-control"></div><div class="col-md-4 bannerDiv"> <textarea class="form-control" name="banner[description][' + i + ']" rows="2"></textarea></div><div class="col-md-3 bannerDiv"> <input type="file" name="banner_image[' + i + ']" class="form-control"/></div><div class="col-md-1 bannerDiv"><button type="button" id="removeBannerRow_' + i + '" onclick="deleteRow(this.id)"><i class="icon-fixed-width icon-trash"></i> Delete</button></div></div>');
//	i++;
//    });
//
//    $('#saveBanner').on("click", function () {
//	$('#bannerArea').find('.fieldArea').each(function () {
//	    status = 1;
//	    $(this).find('.bannerDiv input[type=file]').each(function () {
//		$(this).removeAttr('style');
//		var val = $(this).val();
//		if (val) {
//		} else {
//		    $(this).css("border", "1px solid red");
//		    status = 0;
//		}
//	    });
//	});
//	var banner_val = $('#banner_name').val();
//	$('#banner_name').removeAttr('style');
//	if (!banner_val) {
//	    $('#banner_name').css("border", "1px solid red");
//	    status = 0;
//	}
//
//	if (status == 1) {
//
//	    $('#bannerForm').submit();
//	}
//    });
//});
//
//
//
//$(function () {
//    $("form[name='updatePassword']").validate({
//	rules: {
//	    current_password: "required",
//	    new_password: {
//		required: true,
//		minlength: 6
//	    },
//	    confirm_password: {
//		required: true,
//		minlength: 6,
//		equalTo: "#new_password"
//	    },
//	},
//
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
//
//function stateAutoSelect(countrySelect) {
//
//    var countryId = countrySelect.options[countrySelect.selectedIndex].value;
//
//    $.ajax({
//	type: 'POST',
//	url: site_url + "administrator/get_states",
//	data: {
//	    country_id: countryId
//	},
//	dataType: "text",
//	success: function (result) {
//	    var parsedResult = JSON.parse(result);
//	    console.log(parsedResult);
//	    $('#state_select')
//		    .find('option')
//		    .remove()
//		    .end();
//	    $.each(parsedResult, function (i, obj) {
//		$('#state_select').append($('<option>').text(obj.name).attr('value', obj.id));
//	    });
//	}
//    });
//}
//
//
//
//$(function () {
//    $("form[name='DepotStockAddForm']").validate({
//	rules: {
//	    date: "required",
//	    client: "required",
//	    import_move: "required",
//	    import_reference: "required",
//	    isotank_number: "required",	    
//	    type: "required",
//	    next_inspection_date: "required",
//	    gate_pass: "required",
//	    eir_reference: {
//
//		required: true,
//		//digits: true,
//	    },
//	    tank_status: {
//
//		required: true,
//	    },
//	    equipment_condition: "required",
//	    date_in: "required",
//	    allocated_isotanks: "required",
//	    date_out: "required",
//	    invoicing_date: "required",
//	    number_of_days_bill: {
//		required: true,
//		number: true
//	    },
//
//	},
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
//// $(function() {
//// 	$("#depotDate,#inspectionDate,#DateIn,#DateOut,#invoicingDate").datepicker({ 
//// 		dateFormat: 'Y-m-d' 
//// 	});
//// });
//
///*******************  Previous date disabled *****************/
//
//$(function () {
//    $("#depotDate").datepicker({
//	format: 'dd-mm-yyyy',
//	startDate: '1d',
//	//minDate: 0,
//	onSelect: function () {
//
//	    var startDate = $(this).datepicker('getDate');
//
//	    startDate.setDate(startDate.getDate() + 30);
//	    var minDate = $(this).datepicker('getDate');
//	}
//    });
//});
//
//$(function () {
//    var today = new Date();
//    $('#client_dob').datepicker({
//	format: 'mm-dd-yyyy',
//	autoclose: true,
//	endDate: "-1d",
//	maxDate: today
//    }).on('changeDate', function (ev) {
//	$(this).datepicker('hide');
//    });
//
//
//    $('.datepicker').keyup(function () {
//	if (this.value.match(/[^0-9]/g)) {
//	    this.value = this.value.replace(/[^0-9^-]/g, '');
//	}
//    });
//});
//
//
//$(function () {
//    $("#date_id,#DateIn,#DateOut,#invoicingDate,#ETA,#ETD,#dateInvoice,#imort_Q_validity,#invet_purchase_date,#test_date,#date_in,#wo_date,#inspection_certificate_date,#initial_inspection_date,#last_inspection_date,#next_inspection_date,#estimated_date_sailing,#estimated_date_arrival,#port_depot_out,#storage_depot_in,#inspectionDate,#export_etd,#export_eta,#confirmation_loading_date,#shipment_loading_date,#inspection_cert,#storageFrom,#storageTo").datepicker({
//	format: 'dd-mm-yyyy',
//	//startDate: '1d',
//	//minDate: 0,
//	onSelect: function () {
//
//	    var startDate = $(this).datepicker('getDate');
//
//	    startDate.setDate(startDate.getDate() + 30);
//	    var minDate = $(this).datepicker('getDate');
//	}
//    });
//});
//
//
//$(function () {
//    $(".inventory_date").datepicker({
//	format: 'dd-mm-yyyy',
//	//startDate: '1d',
//	//minDate: 0,
//	onSelect: function () {
//
//	    var startDate = $(this).datepicker('getDate');
//
//	    startDate.setDate(startDate.getDate() + 30);
//	    var minDate = $(this).datepicker('getDate');
//	}
//    });
//});
//
//
//
//
//$(function () {
//    $("#insertIsotankForm").validate({
//	rules: {
//	    import_move_number: {
//		required: true,
//		digits: true,
//		minlength: 5,
//		maxlength: 8
//	    },
//	    skva_file_ref: {
//		required: true,
//	    },
//	    depot_reference: {
//		required: true,
//	    },
//	    isotank_number: {
//		required: true,
//	    },
//	    type: {
//		required: true,
//	    },
//	    inspection_certificate_date: {
//		required: true,
//	    },
//	    initial_inspection_date: {
//		required: true,
//	    },
//	    last_inspection_date: {
//		required: true,
//	    },
//	    next_inspection_date: {
//		required: true,
//	    },
//	    product: {
//		required: true,
//	    },
//	    tank_status: {
//		required: true,
//	    },
//	    volume_capacity: {
//		required: true,
//	    },
//	    port_of_loading: {
//		required: true,
//	    },
//	    vessel_number: {
//		required: true,
//		alpha: true,
//	    },
//	    port_of_discharge: {
//		required: true,
//	    },
//	    bill_number: {
//		required: true,
//	    },
//	    estimated_date_sailing: {
//		required: true,
//	    },
//	    gate_pass_reference: {
//		required: true,
//		digits: true,
//	    },
//	    port_depot_out: {
//		required: true,
//	    },
//	    storage_depot_in: {
//		required: true,
//	    },
//	    estimated_date_arrival: {
//		required: true,
//	    },
//	    remarks: {
//		required: true,
//	    },
//	},
//	messages: {
//	    vessel_number: {
//		alpha: "Only alphabets are allowed."
//	    }
//
//	},
//
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
//
//
//$(function () {
//    $("#workshopForm").validate({
//
//	rules: {
//	    isotank_number: {
//		required: true,
//
//	    },
//	    client_id: {
//		required: true,
//		number: true,
//
//	    },
//	    damage_code: {
//		required: true,
//
//	    },
//	    repair_code: {
//		required: true,
//
//	    },
//	    test_ref: {
//		required: true,
//
//	    },
//	    test_date: {
//		required: true,
//
//	    },
//	    date_in: {
//		required: true,
//
//	    },
//	    ter: {
//		required: true,
//
//	    },
//	    wo: {
//		required: true,
//
//	    },
//	    wo_date: {
//		required: true,
//
//	    },
//	    total_cost: {
//		required: true,
//		number: true,
//
//	    },
//	    total_revenue: {
//		required: true,
//		number: true,
//
//	    },
//
//	    remarks: {
//		required: true,
//
//	    },
//
//	},
//
//	submitHandler: function (form) {
//	    form.submit();
//	}
//
//    });
//});
//
//
//
///*************************  Inventory Management ********************/
//
//
//$(function () {
//    $("#category_inventory").validate({
//		rules: {
//		    product_category: "required",
//		    category_supplier: {
//				required: true,
//		    },
//		    category_date: {
//				required: true,
//		    },
//		    
//		},
//		submitHandler: function (form) {
//		    form.submit();
//		}
//    });
//});
//
//
//$(function () {
//    $("#productInventory").validate({
//		rules: {
//		    item_product_code:{
//		    	required:true,
//		    },
//		    item_category: {
//				required: true,
//		    },
//		    dim_size: {
//				required: true,
//		    },
//		    item_color: {
//				required: true,
//		    },
//		    item_remarks: {
//				required: true,
//		    },
//		    item_product_desc: {
//				required: true,
//		    },
//		    re_print: {
//				required: true,
//		    },
//		    
//		},
//		submitHandler: function (form) {
//		    form.submit();
//		}
//    });
//});
//
//$(function () {
//    $("#stockInventory").validate({
//		rules: {
//		    stock_product_code:{
//		    	required:true,
//		    },
//		    stock_category: {
//				required: true,
//		    },
//		    stock_purchase: {
//				required: true,
//		    },
//		    stock_sold: {
//				required: true,
//		    },
//		    current_stock: {
//				required: true,
//				number:true,
//		    },
//		    stock_unit: {
//				required: true,
//				number:true,
//		    },
//		    stock_sales_amount: {
//				required: true,
//				number:true,
//		    },
//		    stock_rank_quantity: {
//				required: true,
//				digits:true,
//		    },
//		    rank_sales: {
//				required: true,
//				digits:true,
//		    },
//		    stock_product_desc: {
//				required: true,
//		    },
//		    
//		},
//		submitHandler: function (form) {
//		    form.submit();
//		}
//    });
//});
//
//
//$(function () {
//    $("#purchaseInventory").validate({
//		rules: {
//		    purchase_product_code:{
//		    	required:true,
//		    },
//		    puchase_date: {
//				required: true,
//		    },
//		    start_balance: {
//				required: true,
//				number:true,
//		    },
//		    po_order: {
//				required: true,
//		    },
//		    p_supplier: {
//				required: true,
//		    },
//		    p_quantity: {
//				required: true,
//				number:true,
//		    },
//		    p_stock: {
//				required: true,
//				number:true,
//		    },
//		    p_unit_price: {
//				required: true,
//				number:true,
//		    },
//		    p_product_desc: {
//				required: true,
//		    },
//		    
//		},
//		submitHandler: function (form) {
//		    form.submit();
//		}
//    });
//});
//
//$(function () {
//    $("#sales_Price_inventory").validate({
//		rules: {
//		    sales_product_code:{
//		    	required:true,
//		    },
//		    sp_product: {
//				required: true,
//		    },
//		    sp_sales_price: {
//				required: true,
//				number:true,
//		    },
//		},
//		submitHandler: function (form) {
//		    form.submit();
//		}
//    });
//});
//
//$(function () {
//    $("#salesInventory").validate({
//		rules: {
//		    sale_date:{
//		    	required:true,
//		    },
//		    sales: {
//				required: true,
//		    },
//		    sale_product: {
//				required: true,
//		    },
//		    sale_quantity: {
//				required: true,
//				number:true,
//		    },
//		    sale_current_stock: {
//				required: true,
//				number:true,
//		    },
//		    sale_category: {
//				required: true,
//		    },
//		    sale_product_des: {
//				required: true,
//		    },
//		    sale_price_range: {
//				required: true,
//				number:true,
//		    },
//		    sale_unit_range: {
//				required: true,
//				number:true,
//		    },
//
//		    sale_total_amt: {
//				required: true,
//				number:true,
//		    },
//		    sale_remarks: {
//				required: true,
//		    },
//		    
//		},
//		submitHandler: function (form) {
//		    form.submit();
//		}
//    });
//});
//
//
//
//$(function () {
//    $("form[name='InventoryFormss']").validate({
//	rules: {
//	    inventory_cate: "required",
//	    item: {
//		required: true,
//		number: true
//	    },
//	    stock_inventory: "required",
//	    inventory_purchase: {
//		required: true,
//		number: true,
//	    },
//	    sales_price: {
//		required: true,
//		number: true,
//	    },
//	    inventroy_sales: {
//		required: true,
//		number: true,
//	    },
//	    product_code: "required",
//	    puchase_date: "required",
//	    start_balance: {
//		required: true,
//		number: true,
//	    },
//	    invet_price: {
//		required: true,
//		number: true,
//	    },
//	    dim_size: "required",
//	    po_order: "required",
//	    invet_color: "required",
//	    invent_sold: "required",
//	    current_stock: {
//		required: true,
//		number: true
//	    },
//	    re_print: {
//		required: true,
//		//number: true
//	    },
//	    invent_remarks: {
//		required: true,
//		maxlength: 30
//			//number: true
//	    },
//	    stock_unit: {
//		required: true,
//		number: true
//	    },
//	    product_desc: {
//		required: true,
//		maxlength: 100
//	    },
//	    stock_value: {
//		required: true,
//		number: true,
//	    },
//	    price_range: "required",
//	    sales_amount: {
//		required: true,
//		number: true,
//	    },
//	    rank_quantity: "required",
//	    rank_sales: {
//		required: true,
//		number: true,
//	    },
//	    total: "required",
//	    sold_to: "required"
//	},
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
///********************* Client Management ********************/
//
//$(function () {
//    $("form[name='ClientForm']").validate({
//	rules: {
//	    client_code: {
//		required: true,
//		minlength: 5,
//		maxlength: 15
//	    },
//	    client_name: {
//		required: true,
//		alpha: true,
//		minlength: 3,
//		maxlength: 60
//	    },
//	    client_email: {
//		required: true,
//		email: true
//	    },
//	    client_phone: {
//		required: true,
//		digits: true,
//		minlength: 10,
//		maxlength: 15
//
//	    },
//	    client_dob: {
//		required: true,
//
//	    },
//	    country_id: {
//		required: true,
//	    },
//	    state_id: {
//		required: true
//	    },
//	    client_address: {
//		required: true,
//		maxlength: 100,
//
//	    },
//	},
//	messages: {
//	    client_name: {
//		alpha: "Only alphabets are allowed."
//	    }
//	},
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
//
//$(function () {
//    $("#portForm").validate({
//	rules: {
//	    port_number: {
//		required: true,
//		number: true,
//		maxlength: 20
//
//	    },
//	    port_name: {
//		required: true,
//		maxlength: 20
//
//	    },
//	    country_id: {
//		required: true,
//
//	    },
//	    state_id: {
//		required: true,
//
//	    },
//	    port_length: {
//		required: true,
//		maxlength: 20
//
//	    },
//
//	},
//
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
//$(function () {
//    $("#storeForm").validate({
//	rules: {
//	    store_number: {
//		required: true,
//		maxlength: 20
//
//	    },
//	    store_name: {
//		required: true,
//		maxlength: 20
//
//	    },
//	    store_type: {
//		required: true,
//
//	    },
//	    capacity: {
//		required: true,
//		number: true,
//
//	    },
//	    maintenance_cost: {
//		required: true,
//		number: true,
//	    },
//	    country_id: {
//		required: true,
//	    },
//	    state_id: {
//		required: true,
//	    },
//	    //  about: {
//	    // required: true,
//	    // minlength: 3,
//	    // maxlength: 20
//
//	    //  },
//
//	},
//	submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//
//
//
//$(function () {
//    $("#forgotPassword").validate({
//	rules: {
//	    new_password: {
//		required: true,
//		minlength: 6
//	    },
//	    confirm_password: {
//		required: true,
//		minlength: 6,
//		equalTo: "#new_password"
//	    },
//	},
//    });
//});
//
//$(function () {
//    $("#importform").validate({
//	rules: {
//	    tank_status: {
//		required: true,
//	    },
//	    client: {
//		required: true,
//	    },
//	    description: {
//		required: true,
//	    },
//	    code: {
//		required: true,
//	    },
//	    unit_currency: {
//		required: true,
//		number: true,
//	    },
//	},
//    });
//});
//
//
//$(function () {
//    $("#exportform").validate({
//	rules: {
//	    tank_status: {
//		required: true,
//	    },
//	    client: {
//		required: true,
//	    },
//	    description: {
//		required: true,
//	    },
//	    code: {
//		required: true,
//	    },
//	    revenue: {
//		required: true,
//	    },
//	    unit_currency: {
//		required: true,
//		number: true,
//	    },
//	},
//    });
//});
//
//$(function () {
//    $("#importquote").validate({
//	rules: {
//	    port_of_loading: {
//		required: true,
//	    },
//	    destination1: {
//		required: true,
//	    },
//	    client: {
//		required: true,
//	    },
//	    validity: {
//		required: true,
//		number: true,
//	    },
//	    business_route: {
//		required: true,
//		number: true,
//	    },
//	     description: {
//		required: true,
//	    },
//	    code: {
//		required: true,
//	    },
//	    amount: {
//		required: true,
//		number: true,
//	   
//	    }
//	},
//    });
//});
//
//
//$(function () {
//    $("#exportCasting").validate({
//	rules: {	   
//	    exchange_rate: {
//		required: true,
//		number: true,
//	    },
//	    quantity: {
//		required: true,
//		number: true,
//	    },
//	     description: {
//		required: true,		
//	    },    
//	     unit: {
//		required: true,
//		number: true,
//	    },
//	    client: {
//		required: true,
//	    },
//	     supplier: {
//		required: true,
//	    },
//	     client_id: {
//		required: true,
//	    },
//	     cur: {
//		required: true,
//	    },
//	    code: {
//		required: true,
//	    },
//	 	   	   
//	},
//    submitHandler: function (form) {
//	    form.submit();
//	}
//    });
//});
//



//
//
///*************************  Import Costing Shett *********************/
//
//$(function () {
//    $("#importCostingForm").validate({
//	rules: {
//	    import_move_number: {
//		required: true,
//		remote: {
//		    url: site_url + "check_move_number",
//
//		    data: {
//			import_move_number: function () {
//			    var val = $('.automovenumber').val();
//			    return val;
//			}
//		    }
//		}
//	    },
//	    revenue: {
//
//		required: true,
//		//number:true,
//	    },
//	    shipping_line: {
//
//		required: true,
//
//	    },
//	    doc_fee: {
//
//		required: true,
//		number: true,
//
//	    },
//	    shipping_thc: {
//
//		required: true,
//		number: true,
//
//	    },
//	    loading_charge: {
//
//		required: true,
//		number: true
//
//	    },
//	    storage: {
//
//		required: true,
//		number: true
//
//	    },
//	    brokerage: {
//
//		required: true,
//
//	    },
//	    broker_fee: {
//
//		required: true,
//		number: true,
//
//	    },
//	    post_entry: {
//
//		required: true,
//		number: true
//
//	    },
//	    edi_post_entry: {
//
//		required: true,
//		number: true,
//
//	    },
//	    handling: {
//
//		required: true,
//		number: true
//
//	    },
//	    quay_storage: {
//
//		required: true,
//		number: true,
//
//	    },
//	    transport: {
//
//		required: true,
//		number: true,
//
//	    },
//	    other_shipping: {
//
//		number: true,
//	    },
//	    other_shipping: {
//
//		number: true,
//	    },
//	    quay_fee: {
//
//		number: true,
//	    },
//	    brokerage_other: {
//
//		number: true,
//	    },
//
//	},
//	messages: {
//	    import_move_number: {
//		remote: $.validator.format("{0} is already taken."),
//	    }
//
//	},
//
//	submitHandler: function (form) {
//	    //form.submit();
//	    //var formData = $('#importCostingForm').serialize();
//	    //console.log(formData);return false;
//	    $.ajax({
//		url: site_url + 'preview_form',
//		type: 'POST',
//		data: $('#importCostingForm').serialize(),
//		dataType: 'html',
//		success: function (response) {
//		    console.log(response);
//		    $('.modal-body').html(response);
//		    $('#Mymodal').modal('show');
//
//		}
//	    });
//	}
//    });
//});
//
//
//
//function onlyNumber(evt) {
//    evt = (evt) ? evt : window.event;
//    var charCode = (evt.which) ? evt.which : evt.keyCode;
//    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//	return false;
//    }
//    return true;
//}
//
//function isNumber(evt, id, key) {
//    //alert(key+id); return false;
//    //console.log("name"+key);
//    var charCode = (evt.which) ? evt.which : event.keyCode
//    if (charCode == 46) {
//	var inputValue = $("#" + key + id).val();
//	var count = (inputValue.match(/'.'/g) || []).length;
//	if (count < 1) {
//	    if (inputValue.indexOf('.') < 1) {
//		return true;
//	    }
//	    return false;
//	} else {
//	    return false;
//	}
//    }
//    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
//	return false;
//    }
//    return true;
//}
//
//
//$(function () {
//    $(document).on('change', '#costingStatus', function () {
//
//	var statusValue = $(this).val();
//	var import_move_number = $(this).attr('data-number');
//
//	//console.log("vl"+import_move_number);
//	$.ajax({
//	    url: site_url + 'updatecostingstatus',
//	    type: 'POST',
//	    data: {import_move_number: import_move_number, statusValue: statusValue},
//	    dataType: 'json',
//	    success: function (response) {
//		// console.log("dd"+response);
//		location.reload(true);
//	    }
//	});
//    });
//});
//
//
//
//$(function () {
//
//    $("#exportCosting").validate({
//		rules: {
//		    moveNumber: {
//				//required: true,
//				remote: {
//				    url: site_url + "checkMoveNumber",
//				    data: {
//						moveNumber: function () {
//						    var val = $('#movecostingNumber').val();
//						    console.log("isotank" + val);
//						    return val;
//						}
//				    }
//				}
//		    },
//		},
//		messages: {
//		    moveNumber: {
//			remote: $.validator.format("{0} is not a valid move number."),
//		    }
//
//		},
//		submitHandler: function (form) {
//	    	form.submit();
//		}
//    });
//});
//
//
//
//
